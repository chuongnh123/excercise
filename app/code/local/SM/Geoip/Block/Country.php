<?php

/**
 * Class Chuongnh_Geoip_Block_Country
 */
class SM_Geoip_Block_Country extends Mage_Core_Block_Template
{
    public function getCountry()
    {
        $ip = Mage::helper('core/http')->getRemoteAddr();
        if ($ip)
        {
            $result = $this->getIp($ip);
            if (($result->isSuccessful() === true)
                && ($result = json_decode($result->getBody(), true))
                && isset($result['country_name'])
             ) {
                return $result['country_name'];
            }
        }
        return false;
    }

    /**
     * @param $ip
     * @return Zend_Http_Response
     * @throws Zend_Http_Client_Exception
     */
    public function getIp($ip)
    {
        $config = array(
            'adapter' => 'Zend_Http_Client_Adapter_Curl',
            'curloptions' => array(CURLOPT_RETURNTRANSFER => true),
        );
        $client = new Zend_Http_Client('http://freegeoip.net/json/' . $ip, $config);
        $result = $client->request();
        return $result;
    }
}