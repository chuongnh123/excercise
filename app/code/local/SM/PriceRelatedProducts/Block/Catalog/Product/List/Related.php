<?php

/**
 * Class SM_PriceRelatedProducts_Block_Catalog_Product_List_Related
 */
class SM_PriceRelatedProducts_Block_Catalog_Product_List_Related extends Mage_Catalog_Block_Product_List_Related
{
    /**
     * add similarly priced item to related list, if  there is not any
     * @return SM_PriceRelatedProducts_Block_Catalog_Product_List_Related
     */
    protected function _prepareData()
    {
        parent::_prepareData();
        if ($this->_itemCollection->getSize() < 1)
        {
            $product = Mage::registry('product');
            /* @var $product Mage_Catalog_Model_Product */
            $collection = $product->getCollection()
                ->addFieldToFilter('price', array(
                    'gt' => 0
                ))
                ->addFieldToFilter('type_id', 'simple');
            $collection->getSelect()
                ->order(new Zend_Db_Expr('abs(' . $product->getPrice() . '- price)'))
                ->limit(5);
            $this->_itemCollection = $collection;
        }
        return $this;
    }
}